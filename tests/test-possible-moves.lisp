(in-package  "COM.INFORMATIMAGO.GAMES.CHESS")

(defun test/possible-moves ()
  (let ((game  (make-instance 'chess
                   :board (make-board
                           (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black)
                           (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                           (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                           (king   white) (pawn   white) nil nil nil nil  (pawn   black) (king   black)
                           (queen  white) (pawn   white) nil nil nil nil  (pawn   black) (queen  black)
                           (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                           (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                           (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black)))))
    (assert (equal
             ;; rooks
             (list (possible-moves (aref (board game) 0 0)  game 0 0)
                   (possible-moves (aref (board game) 0 7)  game 0 7)
                   (possible-moves (aref (board game) 7 0)  game 7 0)
                   (possible-moves (aref (board game) 7 7)  game 7 7))
             '(NIL NIL NIL NIL))))

  (let ((game (make-instance 'chess
                  :board (make-board
                          (rook   white) nil            nil nil nil nil  (pawn   black) (rook   black)
                          nil            (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                          (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                          (king   white) (pawn   white) nil nil nil nil  (pawn   black) (king   black)
                          (queen  white) (pawn   white) nil nil nil nil  (pawn   black) (queen  black)
                          (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                          (knight white) (pawn   white) nil nil nil nil  (pawn   black) nil
                          (rook   white) (pawn   white) nil nil nil nil  nil            (rook   black)))))
    (assert (equal
             ;; rooks
             (list (possible-moves (aref (board game) 0 0)  game 0 0)
                   (possible-moves (aref (board game) 0 7)  game 0 7)
                   (possible-moves (aref (board game) 7 0)  game 7 0)
                   (possible-moves (aref (board game) 7 7)  game 7 7))
             '(((0 6) (0 5) (0 4) (0 3) (0 2) (0 1) (1 0))
               NIL NIL
               ((7 1) (7 2) (7 3) (7 4) (7 5) (7 6) (6 7))))))


  (let ((game (make-instance 'chess
                  :board (make-board
                          (rook   white) nil            nil nil nil nil  (pawn   black) (rook   black)
                          nil            nil            nil nil nil nil  (pawn   black) (knight black)
                          (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                          (king   white) nil            nil nil nil nil  (pawn   black) (king   black)
                          (queen  white) (pawn   white) nil nil nil nil  nil            (queen  black)
                          (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                          (knight white) (pawn   white) nil nil nil nil  nil             nil
                          (rook   white) (pawn   white) nil nil nil nil  nil            (rook   black)))))
    (assert (equal
             ;; bishops
             (list (possible-moves (aref (board game) 2 0)  game 2 0)
                   (possible-moves (aref (board game) 5 0)  game 5 0)
                   (possible-moves (aref (board game) 2 7)  game 2 7)
                   (possible-moves (aref (board game) 5 7)  game 5 7))
             '(((0 2) (1 1) (7 5) (6 4) (5 3) (4 2) (3 1))
               NIL
               NIL
               ((0 2) (1 3) (2 4) (3 5) (4 6) (7 5) (6 6))))))



  #-(and)
  ;; knights
  (list (possible-moves (aref (board game) 1 0)  game 1 0)
        (possible-moves (aref (board game) 6 0)  game 6 0)
        (possible-moves (aref (board game) 1 7)  game 1 7)
        (possible-moves (aref (board game) 6 7)  game 6 7))

  :success)

(test/possible-moves)
