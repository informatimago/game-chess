;;;; -*- mode:lisp;coding:utf-8 -*-
(in-package  "COM.INFORMATIMAGO.GAMES.CHESS")


;; https://pasteboard.co/iltnVJyMVT3E.png

(assert (equal
  (with-output-to-string (*standard-output*)
    (write-line "-*- mode:text;eval:(pjb-ansi-colorize-buffer) -*-" )
    (let ((board (make-board
                  (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black)
                  (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                  (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                  (king   white) (pawn   white) nil nil nil nil  (pawn   black) (king   black)
                  (queen  white) (pawn   white) nil nil nil nil  (pawn   black) (queen  black)
                  (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                  (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                  (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black))))
      (terpri)
      (print-board board :ecma-48-black-white t)
      (terpri)
      (print-board board :ecma-48-red-blue t)
      (terpri)
      (print-board board :unicode t)))

  "-*- mode:text;eval:(pjb-ansi-colorize-buffer) -*-

[30;42m 　 [30;42m Ａ [30;42m Ｂ [30;42m Ｃ [30;42m Ｄ [30;42m Ｅ [30;42m Ｆ [30;42m Ｇ [30;42m Ｈ [30;42m 　 [0m
[30;42m ８ [30;43m ♜ [30;46m ♞ [30;43m ♝ [30;46m ♚ [30;43m ♛ [30;46m ♝ [30;43m ♞ [30;46m ♜ [30;42m ８ [0m
[30;42m ７ [30;46m ♟ [30;43m ♟ [30;46m ♟ [30;43m ♟ [30;46m ♟ [30;43m ♟ [30;46m ♟ [30;43m ♟ [30;42m ７ [0m
[30;42m ６ [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;42m ６ [0m
[30;42m ５ [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;42m ５ [0m
[30;42m ４ [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;42m ４ [0m
[30;42m ３ [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;46m 　 [30;43m 　 [30;42m ３ [0m
[30;42m ２ [37;43m ♟ [37;46m ♟ [37;43m ♟ [37;46m ♟ [37;43m ♟ [37;46m ♟ [37;43m ♟ [37;46m ♟ [30;42m ２ [0m
[30;42m １ [37;46m ♜ [37;43m ♞ [37;46m ♝ [37;43m ♚ [37;46m ♛ [37;43m ♝ [37;46m ♞ [37;43m ♜ [30;42m １ [0m
[30;42m 　 [30;42m Ａ [30;42m Ｂ [30;42m Ｃ [30;42m Ｄ [30;42m Ｅ [30;42m Ｆ [30;42m Ｇ [30;42m Ｈ [30;42m 　 [0m

[30;42m 　 [30;42m Ａ [30;42m Ｂ [30;42m Ｃ [30;42m Ｄ [30;42m Ｅ [30;42m Ｆ [30;42m Ｇ [30;42m Ｈ [30;42m 　 [0m
[30;42m ８ [31;47m ♜ [31;40m ♞ [31;47m ♝ [31;40m ♚ [31;47m ♛ [31;40m ♝ [31;47m ♞ [31;40m ♜ [30;42m ８ [0m
[30;42m ７ [31;40m ♟ [31;47m ♟ [31;40m ♟ [31;47m ♟ [31;40m ♟ [31;47m ♟ [31;40m ♟ [31;47m ♟ [30;42m ７ [0m
[30;42m ６ [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [30;42m ６ [0m
[30;42m ５ [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [30;42m ５ [0m
[30;42m ４ [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [30;42m ４ [0m
[30;42m ３ [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [31;40m 　 [31;47m 　 [30;42m ３ [0m
[30;42m ２ [36;47m ♟ [36;40m ♟ [36;47m ♟ [36;40m ♟ [36;47m ♟ [36;40m ♟ [36;47m ♟ [36;40m ♟ [30;42m ２ [0m
[30;42m １ [36;40m ♜ [36;47m ♞ [36;40m ♝ [36;47m ♚ [36;40m ♛ [36;47m ♝ [36;40m ♞ [36;47m ♜ [30;42m １ [0m
[30;42m 　 [30;42m Ａ [30;42m Ｂ [30;42m Ｃ [30;42m Ｄ [30;42m Ｅ [30;42m Ｆ [30;42m Ｇ [30;42m Ｈ [30;42m 　 [0m

　　　 　Ａ　 　Ｂ　 　Ｃ　 　Ｄ　 　Ｅ　 　Ｆ　 　Ｇ　 　Ｈ　
　８　|＿♜＿|＿♞＿|＿♝＿|＿♚＿|＿♛＿|＿♝＿|＿♞＿|＿♜＿|　８　
　７　|＿♟＿|＿♟＿|＿♟＿|＿♟＿|＿♟＿|＿♟＿|＿♟＿|＿♟＿|　７　
　６　|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|　６　
　５　|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|　５　
　４　|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|　４　
　３　|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|＿＿＿|　３　
　２　|＿♙＿|＿♙＿|＿♙＿|＿♙＿|＿♙＿|＿♙＿|＿♙＿|＿♙＿|　２　
　１　|＿♖＿|＿♘＿|＿♗＿|＿♔＿|＿♕＿|＿♗＿|＿♘＿|＿♖＿|　１　
　　　 　Ａ　 　Ｂ　 　Ｃ　 　Ｄ　 　Ｅ　 　Ｆ　 　Ｇ　 　Ｈ　
"))


(assert (equal (with-output-to-string (*standard-output*)
                 (dopieces (p (c r)
                              (make-board
                               (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black)
                               (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                               (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                               (king   white) (pawn   white) nil nil nil nil  (pawn   black) (king   black)
                               (queen  white) (pawn   white) nil nil nil nil  (pawn   black) (queen  black)
                               (bishop white) (pawn   white) nil nil nil nil  (pawn   black) (bishop black)
                               (knight white) (pawn   white) nil nil nil nil  (pawn   black) (knight black)
                               (rook   white) (pawn   white) nil nil nil nil  (pawn   black) (rook   black)))
                           (format t "~A~A: ~A; "
                                   (aref "ABCDEFGH" c)
                                   (aref "12345678" r)
                                   (get-piece-string  p :unicode))))
               "A1: ♖; B1: ♘; C1: ♗; D1: ♔; E1: ♕; F1: ♗; G1: ♘; H1: ♖; A2: ♙; B2: ♙; C2: ♙; D2: ♙; E2: ♙; F2: ♙; G2: ♙; H2: ♙; A7: ♟; B7: ♟; C7: ♟; D7: ♟; E7: ♟; F7: ♟; G7: ♟; H7: ♟; A8: ♜; B8: ♞; C8: ♝; D8: ♚; E8: ♛; F8: ♝; G8: ♞; H8: ♜; "))
