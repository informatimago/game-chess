;;;; -*- mode:lisp;coding:utf-8 -*-

(asdf:defsystem :com.informatimago.game.chess
    :name "Chess Game Stuff"
    :description  "Chess Game Stuff"
    :author "<PJB> Pascal Bourguignon <pjb@informatimago.com"
    :version "0.0.2"
    :licence "AGPL-3"
    :properties ((#:author-email                   . "pjb@informatimago.com")
                 (#:date                           . "Automn 2021")
                 ((#:albert #:output-dir)          . "../documentation/com.informatimago.game.chess/")
                 ((#:albert #:formats)             . ("docbook"))
                 ((#:albert #:docbook #:template)  . "book")
                 ((#:albert #:docbook #:bgcolor)   . "white")
                 ((#:albert #:docbook #:textcolor) . "black"))
    :depends-on (;; "split-sequence"
                 ;; "hunchentoot"
                 ;; "bordeaux-threads"
                 "com.informatimago.common-lisp")
    #+asdf-unicode :encoding #+asdf-unicode :utf-8
    :components ((:file "sources/packages")
                 (:file "sources/board"            :depends-on ("sources/packages"))
                 (:file "sources/chess"            :depends-on ("sources/packages" "sources/board"))))


;;;; THE END ;;;;
