(in-package  "COM.INFORMATIMAGO.GAMES.CHESS")

(defclass chess ()
  ((board :initarg :board :initform (make-board) :reader board)))


(defgeneric coalesce (chess)
  (:documentation "Freeze the pieces into the board."))

(defmethod coalesce ((chess chess))
  (dopieces (p (c r) (board chess))
            (setf (col p) c
                  (row p) r)))


(defgeneric possible-movies (piece game))

(defmethod possible-moves ((empty null) (game chess))
  (declare (ignorable empty game))
  '())

(defmethod possible-moves ((piece rook) (game chess))
  (let ((board (board game))
        (moves '())
        (col (col piece))
        (row (row piece)))
    (loop
       :for i :in (append (loop :for i :from (1+ col) :to     7 :collect i)
                          (loop :for i :from (1- col) :downto 0 :collect i))
       :do (cond
             ((null (aref board i row))
              (push (list i row) moves))
             ((eql (color piece) (color (aref board i row)))
              (loop-finish))
             (t
              (push (list i row) moves)
              (loop-finish))))
    (loop
       :for j :in (append (loop :for j :from (1+ row) :to     7 :collect j)
                          (loop :for j :from (1- row) :downto 0 :collect j))
       :do (cond
             ((null (aref board col j))
              (push (list col j) moves))
             ((eql (color piece) (color (aref board col j)))
              (loop-finish))
             (t
              (push (list col j) moves)
              (loop-finish))))
    moves))

(defmethod possible-moves ((piece bishop) (game chess))
  (let ((board (board game))
        (moves '())
        (col (col piece))
        (row (row piece)))
    (loop
       :for (i j)
       :in (append (loop
                      :for i :from (1+ col) :to 7
                      :for j :from (1+ row) :to 7
                      :collect (list i j))
                   (loop
                      :for i :from (1+ col) :to 7
                      :for j :from (1- row) :downto 0
                      :collect (list i j))
                   (loop
                      :for i :from (1- col) :downto 0
                      :for j :from (1+ row) :to 7
                      :collect (list i j))
                   (loop
                      :for i :from (1- col) :downto 0
                      :for j :from (1- row) :downto 0
                      :collect (list i j)))
       :do (cond
             ((null (aref board i j))
              (push (list i j) moves))
             ((eql (color piece) (color (aref board i j)))
              (loop-finish))
             (t
              (push (list i j) moves)
              (loop-finish))))
    moves))
