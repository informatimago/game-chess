;;;; -*- mode:lisp;coding:utf-8 -*-
(in-package  "COM.INFORMATIMAGO.GAMES.CHESS")

;;;
;;; Boards are mere 2D arrays (column row)
;;; NIL is empty cell, an object is a piece.

(defun make-board (&rest pieces)
  (let ((board (make-array '(8 8) :initial-element nil)))
    (replace (make-array (* 8 8) :displaced-to board) pieces)
    board))

;;;
;;; Macros
;;;

(defmacro docells (((column row) board &optional result) &body body)
  (let ((b (gensym)) (c (gensym)) (r (gensym)))
    `(loop
        :with ,b := ,board
        :for ,r :below (array-dimension ,b 1)
        :do (loop
               :named ,(gensym)
               :for ,c :below (array-dimension ,b 0)
               :do (let ((,column ,c)
                         (,row ,r))
                     ,@body))
        :finally (return ,result))))

(defmacro dopieces ((piece (column row) board &optional result) &body body)
  (let ((b (gensym)) (p (gensym)))
    `(let ((,b ,board))
       (docells ((,column ,row) ,b ,result)
                (let ((,piece (aref ,b ,column ,row)))
                  (when ,piece
                    (locally
                        ,@body)))))))


;;;
;;; Chess Pieces
;;;

(defconstant white 'white)
(defconstant black 'black)

(defclass piece ()
  ((color :initarg :color :reader color)
   (col   :initarg :col :initform nil :accessor col)
   (row   :initarg :row :initform nil :accessor row)))

(defclass king   (piece) ())
(defclass rook   (piece) ())
(defclass bishop (piece) ())
(defclass queen  (piece) ())
(defclass knight (piece) ())
(defclass pawn   (piece) ())

(defun king   (color) (make-instance 'king   :color color))
(defun rook   (color) (make-instance 'rook   :color color))
(defun bishop (color) (make-instance 'bishop :color color))
(defun queen  (color) (make-instance 'queen  :color color))
(defun knight (color) (make-instance 'knight :color color))
(defun pawn   (color) (make-instance 'pawn   :color color))



;;;
;;; Printing/drawing boards
;;;

(defparameter *board-styles* '(:ecma-48-red-blue :ecma-48-black-white :unicode)
  "A list of board printing styles.
If methods for new styles are defined,
the style object should be added to this list.")

(defgeneric get-piece-string (piece style))
(defgeneric print-board (board style stream))

(defconstant ESC         (code-char 27))
(defconstant WIDE-SPACE  "　")
(defconstant +X+         "Ｘ")


(defmethod get-piece-string ((piece null)  (style t))
  WIDE-SPACE)

(defmethod get-piece-string ((piece piece) (style t))
  (let* ((map    #("♖♘♗♕♔♙"             ; whites
                   "♜♞♝♛♚♟"             ; blacks
                   ))
         (pieces (aref map (if (eq (color piece) white) 0 1))))
    (string (aref pieces (etypecase piece
                           (rook   0)
                           (knight 1)
                           (bishop 2)
                           (queen  3)
                           (king   4)
                           (pawn   5))))))


(defconstant +black+   0)
(defconstant +red+     1)
(defconstant +green+   2)
(defconstant +yellow+  3)
(defconstant +blue+    4)
(defconstant +magenta+ 5)
(defconstant +cyan+    6)
(defconstant +white+   7)
(defun fore (color-code) (+ 30 color-code))
(defun back (color-code) (+ 40 color-code))


(defmethod print-board ((board array) (style (eql :ecma-48-black-white)) (stream t))
  (let ((*print-circle*    nil)
        (white-empty       (format nil "~C[~D;~Dm" ESC (fore +white+) (back +yellow+)))
        (black-empty       (format nil "~C[~D;~Dm" ESC (fore +black+) (back +cyan+)))
        (white-white-piece (format nil "~C[~D;~Dm" ESC (fore +white+) (back +yellow+)))
        (white-black-piece (format nil "~C[~D;~Dm" ESC (fore +white+) (back +cyan+)))
        (black-white-piece (format nil "~C[~D;~Dm" ESC (fore +black+) (back +yellow+)))
        (black-black-piece (format nil "~C[~D;~Dm" ESC (fore +black+) (back +cyan+)))
        (normal            (format nil "~C[0m"     ESC))
        (border            (format nil "~C[~D;~Dm" ESC (fore +black+) (back +green+)))
        (map               #("♖♘♗♕♔♙"
                             "♜♞♝♛♚♟"))
        (rows              "１２３４５６７８")
        (columns           "ＡＢＣＤＥＦＧＨ")
        (corner            WIDE-SPACE)
        (empty             "＿")
        (cell              "~A ~A "))
    (flet ((hborder ()
             (loop
                :for column :below 8
                :initially (format t cell border corner)
                :do (format t cell border (aref columns column))
                :finally (format t cell border corner)
                  (format t "~A~%" normal))))
      (hborder)
      (loop
         :for row :from 7 :downto 0
         :do (format t cell border (aref rows row))
           (loop
              :for column :below 8
              :for piece := (get-piece-string (aref board column row) style)
              :for white-pos := (position (aref piece 0) (aref map 0))
              :for dpiece := (cond
                               ((null piece) (aref empty 0))
                               (white-pos    (aref (aref map 1) white-pos))
                               (t            piece))
              :do (format t cell (if (evenp (+ row column))
                                     (if (null piece)
                                         black-empty
                                         (if white-pos
                                             white-black-piece
                                             black-black-piece))
                                     (if (null piece)
                                         white-empty
                                         (if white-pos
                                             white-white-piece
                                             black-white-piece)))
                          dpiece))
           (format t cell border (aref rows row))
           (format t "~A~%" normal))
      (hborder))))

(defmethod print-board ((board array) (style (eql :ecma-48-red-blue)) (stream t))
  (let ((*print-circle*    nil)
        (white-empty       (format nil "~C[~D;~Dm" ESC (fore +white+) (back +white+)))
        (black-empty       (format nil "~C[~D;~Dm" ESC (fore +black+) (back +black+)))
        (white-white-piece (format nil "~C[~D;~Dm" ESC (fore +cyan+)  (back +white+)))
        (white-black-piece (format nil "~C[~D;~Dm" ESC (fore +cyan+)  (back +black+)))
        (black-white-piece (format nil "~C[~D;~Dm" ESC (fore +red+)   (back +white+)))
        (black-black-piece (format nil "~C[~D;~Dm" ESC (fore +red+)   (back +black+)))
        (normal            (format nil "~C[0m"     ESC))
        (border            (format nil "~C[~D;~Dm" ESC (fore +black+) (back +green+)))
        (map               #("♖♘♗♕♔♙"
                             "♜♞♝♛♚♟"))
        (rows              "１２３４５６７８")
        (columns           "ＡＢＣＤＥＦＧＨ")
        (corner            WIDE-SPACE)
        (empty             "＿")
        (cell              "~A ~A "))
    (flet ((hborder ()
             (loop
                :for column :below 8
                :initially (format t cell border corner)
                :do (format t cell border (aref columns column))
                :finally (format t cell border corner)
                  (format t "~A~%" normal))))
      (hborder)
      (loop
         :for row :from 7 :downto 0
         :do (format t cell border (aref rows row))
           (loop
              :for column :below 8
              :for piece := (get-piece-string (aref board column row) style)
              :for white-pos := (position (aref piece 0) (aref map 0))
              :for dpiece := (cond
                               ((null piece) (aref empty 0))
                               (white-pos    (aref (aref map 1) white-pos))
                               (t            piece))
              :do (format t cell (if (evenp (+ row column))
                                     (if (null piece)
                                         black-empty
                                         (if white-pos
                                             white-black-piece
                                             black-black-piece))
                                     (if (null piece)
                                         white-empty
                                         (if white-pos
                                             white-white-piece
                                             black-white-piece)))
                          dpiece))
           (format t cell border (aref rows row))
           (format t "~A~%" normal))
      (hborder))))


(defparameter *cell-format*  "~1A")
(defparameter *empty*        "＿")
(defparameter *space*        "　")
(defparameter *line*         "＿")
(defparameter *chess-symbols*
  '(((king        white) . "♔")
    ((queen       white) . "♕")
    ((rook        white) . "♖")
    ((bishop      white) . "♗")
    ((knight      white) . "♘")
    ((pawn        white) . "♙")
    ((king        black) . "♚")
    ((queen       black) . "♛")
    ((rook        black) . "♜")
    ((bishop      black) . "♝")
    ((knight      black) . "♞")
    ((pawn        black) . "♟")))

(defmethod get-piece-string ((piece piece) (style (eql :unicode)))
  (cdr (assoc (list (class-name (class-of piece)) (color piece))
              *chess-symbols*
              :test (function equal))))

(defmethod print-board ((board array) (style (eql :unicode)) (stream t))
  (let ((*print-circle*    nil)
        (rows              "１２３４５６７８")
        (columns           "ＡＢＣＤＥＦＧＨ")
        ;; (corner            "Ｘ")
        (corner            "　"))
    (flet ((letters ()
             (loop
                :for i :below 8
                :initially (format stream "~A~A~A" *space* corner *space* )
                :do (format stream " ~A~?~A" *space* *cell-format* (list (aref columns i)) *space* )
                :finally (format stream "~%"))))
      (letters)
      (loop
         :for j :from 7 :downto 0
         :do (loop
                :for i :below 8
                :initially (format stream "~A~A~A" *space* (aref rows j) *space*)
                :do (if (aref board i j)
                        (format stream "|~A~A~A" *line* (get-piece-string (aref board i j) style) *line* )
                        (format stream "|~A~A~A" *line* *empty* *line* ))
                :finally (format stream "|~A~A~A~%" *space* (aref rows j) *space*)))
      (letters)
      (values))))


;;;; THE END ;;;;
